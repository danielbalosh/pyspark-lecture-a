import shutil

from pyspark import Row
from pyspark.sql import SparkSession

from src.jobs.sum.job import main
from tests.test_utils.test_spark import spark_session  # noqa: F401


def _set_paths_vars():
    input_path = '/tmp/input/'
    output_path = '/tmp/output/'
    return input_path, output_path


def _delete_input_output_errors(input_path, output_path):
    shutil.rmtree(input_path, ignore_errors=True)
    shutil.rmtree(output_path, ignore_errors=True)


def _create_csv_input(input_path, spark_session):  # noqa: F811
    # noinspection DuplicatedCode
    input_df = spark_session.createDataFrame([
        Row(version='1', ip="208.80.152.201"),
        Row(version='1', ip="208.80.152.202"),
        Row(version='1', ip="208.80.152.203"),
        Row(version='1', ip="208.80.152.204"),
    ])
    input_df.write.csv(input_path, header=True)


def test_integration(spark_session: SparkSession):  # noqa: F811
    input_path, output_path = _set_paths_vars()

    _delete_input_output_errors(input_path, output_path)

    _create_csv_input(input_path, spark_session)

    main(spark=spark_session, input_path=input_path, output_path=output_path)

    output = spark_session.read.csv(output_path, header=True)
    assert output.count() == 4
    assert output.collect()[0].version == '1'
