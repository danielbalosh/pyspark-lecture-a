#!/bin/bash
printf "\n# set ~/work to both work dir and PYTHONPATH \n\n"
echo "cd /home/jovyan/work/" >>/home/jovyan/.bashrc
echo "export PYTHONPATH=$PYTHONPATH:/home/jovyan/work/" >>/home/jovyan/.bashrc
printf "\n# run.sh running pip install\n\n"
pip install -r /home/jovyan/work/requirements.txt
printf "\n# run.sh running exec to replace to jupyter lab process\n\n"
exec /bin/bash /usr/local/bin/start.sh jupyter lab
