from pyspark.sql import SparkSession


def _foo():
    pass


def main(spark: SparkSession, input_path, output_path):
    df = spark.read.csv(input_path, header=True)
    df.write.csv(output_path, header=True)
