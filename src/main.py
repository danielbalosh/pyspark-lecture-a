import src.jobs.sum.job
import time

import pyspark


def get_spark_session(app_name=None):
    """ get or create a spark session based on parameters given in <config>"""
    spark_conf = pyspark.SparkConf().setAppName(app_name)
    # get or create spark session
    spark = pyspark.sql.SparkSession.builder.config(conf=spark_conf).getOrCreate()
    # Just report errors, no warnings.
    spark.sparkContext.setLogLevel('ERROR')
    return spark


if __name__ == '__main__':
    JOB_NAME = 'pyspark-lecture-a'
    spark = get_spark_session(app_name=JOB_NAME)

    start = time.time()
    src.jobs.sum.job.run(spark=spark, input_path='/tmp/input', output_path='/tmp/output')
    end = time.time()

    print("\nExecution of job %s took %s seconds" % (JOB_NAME, end - start))
