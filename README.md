# setup

clone to hardcoded tmp
```shell script
git clone https://gitlab.com/danielbalosh/pyspark-lecture-a.git /tmp/pyspark-lecture-a/
```

# run
- Run using docker run command.
    - Require ability to mount /tmp, On mac you might need to add this to docker desktop preferences. ![](img/mounts-macos.png)
    - run: 
      ```shell script
      cd /tmp/pyspark-lecture-a/ && chmod +x /tmp/pyspark-lecture-a/docker/run.sh && docker run --rm  --name lab -p 8888:8888 -e JUPYTER_ENABLE_LAB=yes -v /tmp/pyspark-lecture-a/:/home/jovyan/work jupyter/pyspark-notebook:1386e2046833 /home/jovyan/work/docker/run.sh
      ```

- open url given at terminal with browser: ![](img/lab-url.png) 

# tests

- open terminal inside lab: ![](img/terminal-at-lab.png)
- run and see tests pass:
  ```shell script
  pytest -vv  -Wignore::DeprecationWarning  -Wignore::RuntimeWarning
  ```


# steps

## 1. change column using withColumn function

- replace the integration test at [tests/src/jobs/sum/test_job.py](tests/src/jobs/sum/test_job.py) with:
```python
def test_integration(spark_session: SparkSession):  # noqa: F811
    input_path, output_path = _set_paths_vars()

    _delete_input_output_errors(input_path, output_path)

    _create_csv_input(input_path, spark_session)

    main(spark=spark_session, input_path=input_path, output_path=output_path)

    output = spark_session.read.csv(output_path, header=True)
    assert output.count() == 4
    assert output.collect()[0].version == '2'
    assert output.collect()[1].version == '2'
    assert output.collect()[2].version == '2'
    assert output.collect()[3].version == '2'

```

- run the test, see it fails
- write your code at [src/jobs/sum/job.py](src/jobs/sum/job.py) to make the test pass
- help: https://lmgtfy.com/?q=dataframe+withcolumn+pyspark+example


## 2. create column from other column using withColumn function

The feature needed is create md5 column calculated from the ip column.

- replace the integration test at [tests/src/jobs/sum/test_job.py](tests/src/jobs/sum/test_job.py) with:
```python
def test_integration(spark_session: SparkSession):  # noqa: F811
    input_path, output_path = _set_paths_vars()

    _delete_input_output_errors(input_path, output_path)

    _create_csv_input(input_path, spark_session)

    main(spark=spark_session, input_path=input_path, output_path=output_path)

    output = spark_session.read.csv(output_path, header=True)
    assert output.count() == 4

    assert output.collect()[0].version == '1'
    assert output.where(output.ip == '208.80.152.201').collect()[0].md5 == '59907a259b8bb4a56af9f80ad1dfa6bd'
    assert output.where(output.ip == '208.80.152.202').collect()[0].md5 == 'e10f14496a817298bad0d72c1fc3533e'
```
- run the test, see it fails
- write your code at [src/jobs/sum/job.py](src/jobs/sum/job.py) to make the test pass
- help: https://spark.apache.org/docs/latest/api/python/pyspark.sql.html#pyspark.sql.functions.hash


## 3. create column from other column using rdd map

Now usage of withColumn is forbidden, instead use rdd map. 

- use the same test from step 2
- run the test, see it passes
- write your code at [src/jobs/sum/job.py](src/jobs/sum/job.py) to make the test pass, 
  this time remove withColumn and use:
  ```python
  # df -> rdd -> map -> back to df
  spark.createDataFrame(df.rdd.map( _foo )))

  # create new row with added column
  Row(**line_inside_rdd_map.asDict(), newcolumn='value')

  # md5 in python
  hashlib.md5('string'.encode()).hexdigest()
  ```

## 4. create column from other column using withColumn and UDF

Now use withColumn that calls UDF. 

- use the same test from step 2
- run the test, see it passes
- write your code at [src/jobs/sum/job.py](src/jobs/sum/job.py) to make the test pass, 
  this time remove UDF and use:
  ```python
  from pyspark.sql.functions import udf
  ### create the udf
  ### call withColumn using the udf
  ```
- help: https://lmgtfy.com/?q=pyspark+udf+withcolumn
